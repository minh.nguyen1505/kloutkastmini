export function setHeightScroll() {
    const scrollBlock = document.querySelector('.-able-scroll-');
    function scrollFunc() {
        let setTop = scrollBlock.offsetTop;
        scrollBlock.style.cssText = 'height: calc(100vh - ' + setTop + 'px)';
    }
    if (scrollBlock) {
        scrollFunc();
        window.addEventListener('resize', scrollFunc);
        window.addEventListener('hashchange', scrollFunc);
    }
}