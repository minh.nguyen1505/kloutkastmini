import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Introduce from "./views/Introduce.vue";
import SignUp from "./views/SignUp.vue";
import Profile from "./views/Profile.vue";
import EditProfile from "./views/EditProfile.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/introduce",
      name: "introduce",
      component: Introduce
    },
    {
      path: "/signup",
      name: "signup",
      component: SignUp
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile
    },
    {
      path: "/edit-profile",
      name: "edit-profile",
      component: EditProfile
    },    
  ]
});
