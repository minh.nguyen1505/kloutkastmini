import DefaultLayout from '../layouts/DefaultLayout';
import Home from '../views/Home';
import Introduce from '../views/Introduce';
import SignUp from '../views/SignUp';
import Profile from '../views/Profile';

const children = [
  {
    path: 'home',
    name: 'home',
    meta: {
      label: 'Home',
      title: 'Home'
    },
    component: Home
  },
  {
    path: 'profile',
    name: 'profile',
    meta: {
      label: 'Your Profile',
      title: 'Your Profile'
    },
    props: true,
    component: Profile
  },
  {
    path: 'signup',
    name: 'signup',
    meta: {
      label: 'Signup for Klout Kast',
      title: 'Signup for Klout Kast'
    },
    props: true,
    component: SignUp
  }
];

const routes = [
  {
    path: '/',
    component: DefaultLayout,
    redirect: '/introduce',
    meta: {
      label: 'DefaultLayout'
    },
    children
  },
  {
    path: '/introduce',
    name: 'introduce',
    meta: {
      label: 'Introduce',
      title: 'Introduce'
    },
    component: Introduce
  }
];

export default routes;
